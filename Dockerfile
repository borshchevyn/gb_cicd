FROM httpd:2.4.48

WORKDIR /usr/local/apache2/htdocs

COPY web/index.html /usr/local/apache2/htdocs/index.html

EXPOSE 80

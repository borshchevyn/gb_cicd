---
image: alpine:3.13.5

variables:
  APP_REVISION: "1.11"
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  REGISTRY_URL: $CI_REGISTRY
  IMAGE_TAG: $CI_COMMIT_BRANCH-$CI_COMMIT_SHORT_SHA
  IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$CI_COMMIT_SHORT_SHA
  IMAGE_LATEST: $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION
  PIPELINE_REF: main
  PIPELINE_STATUS: running

before_script:
  - docker login -u $GITLAB_CI_USER -p $GITLAB_CI_PASSWORD $REGISTRY_URL

stages:
  - cleanup 
  - build
  - test
  - deploy

.base_deploy: &base_deploy
  stage: deploy
  tags:
    - kubernetes
  before_script:
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
  script:
    - echo $KUBECONFIG
    - export KUBECONFIG=$KUBECONFIG
    - ./kubectl create secret docker-registry gitlab-registry --docker-server=$REGISTRY_URL --docker-username=$GITLAB_CI_USER --docker-password=$GITLAB_CI_PASSWORD --dry-run -o yaml | ./kubectl apply -f -
    - sed -i "s/CI_ENVIRONMENT_SLUG/${CI_ENVIRONMENT_SLUG}/g" ./manifests/webapp/deployment.yaml
    - sed -i "s/CI_PROJECT_PATH_SLUG/${CI_PROJECT_PATH_SLUG}/g" ./manifests/webapp/deployment.yaml
    - sed -i "s|IMAGE|${IMAGE_LATEST}|g" ./manifests/webapp/deployment.yaml
    - ./kubectl apply -f ./manifests/webapp/deployment.yaml
    - sed -i "s/CI_ENVIRONMENT_SLUG/${CI_ENVIRONMENT_SLUG}/g" ./manifests/webapp/service.yaml
    - sed -i "s/CI_PROJECT_PATH_SLUG/${CI_PROJECT_PATH_SLUG}/g" ./manifests/webapp/service.yaml
    - sed -i "s/CI_ENVIRONMENT_SLUG/${CI_ENVIRONMENT_SLUG}/g" ./manifests/webapp/ingress.yaml
    - ./kubectl apply -f ./manifests/webapp/service.yaml
    - ./kubectl apply -f ./manifests/webapp/ingress.yaml

cleanup_pipeline:
  stage: cleanup
  tags:
    - kubernetes
  before_script:
    - apk add --no-cache curl
    - apk add --no-cache jq
  script:
    - >
      for p_id in $(curl -H "Content-Type:application/json" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines?ref=$PIPELINE_REF&status=$PIPELINE_STATUS" | jq '.[] | .id'); do
        if [ $p_id != $CI_PIPELINE_ID ]; then
          echo "Cancel pipeline id: ${p_id}"
          curl --request POST --header "Private-Token: $CI_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$p_id/cancel"
        fi
      done

build_webapp:
  image: docker:18.09.9
  stage: build
  services:
    - docker:18.09.9-dind
  tags:
    - kubernetes
  script:
    #- docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION || true
    #- docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION -t $CI_REGISTRY_IMAGE:$IMAGE_TAG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION .
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION .
    - docker push $CI_REGISTRY_IMAGE:$IMAGE_TAG
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION
  only:
    changes:
      - "Dockerfile"
      - "web/*"
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

test_webapp:
  stage: test
  image: docker:18.09.9
  services:
    - docker:18.09.9-dind
  tags:
    - kubernetes
  before_script:
    - apk add --no-cache curl
  after_script:
    - docker rm -f $(docker ps -aq)
  script:
    - docker run -d -p 8080:80 $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH-$APP_REVISION
    - sleep 15
    - curl http://docker:8080 | grep $APP_REVISION
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /skip/
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

deploy_webapp_stage:
  <<: *base_deploy
  environment: 
    name: staging
    on_stop: stop_stage_env
    auto_stop_in: 1 day
  variables:
    WEB_APP_DB_SERVER_NAME: stage_db
    TARGET_ENV: staging
  only:
    refs:
      - main

deploy_webapp_prod:
  <<: *base_deploy
  environment: 
    name: production
  variables:
    WEB_APP_DB_SERVER_NAME: prod_db
  when: manual
  only:
    refs:
      - main

stop_stage_env:
  stage: deploy
  variables:
    TARGET_ENV: staging
  script:
    - echo "STOP ${TARGET_ENV}"
  only:
    - main
  when: manual
  environment:
    name: staging
    action: stop

